// Import bộ thư viện express
const express = require('express'); 

const router = express.Router();

router.get("/reviews", (request, response) => {
    response.json({
        message: "GET All Review"
    })
});

router.post("/reviews", (request, response) => {
    response.json({
        message: "POST new Review"
    })
});

router.get("/reviews/:reviewId", (request, response) => {
    let reviewId = request.params.reviewId;

    response.json({
        message: "GET review ID = " + courseId
    })
});

router.put("/reviews/:reviewId", (request, response) => {
    let reviewId = request.params.reviewId;

    response.json({
        message: "PUT review ID = " + reviewId
    })
});

router.delete("/reviews/:reviewId", (request, response) => {
    let reviewId = request.params.reviewId;

    response.json({
        message: "DELETE review ID = " + reviewId
    })
});

// Export dữ liệu thành 1 module
module.exports = router;